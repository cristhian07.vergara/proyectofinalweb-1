import { Injectable } from '@angular/core';
import { Firestore, collectionData, collection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';




@Injectable({
    providedIn: 'root'
  })
  export class PacientesService {
  
    constructor(private firestore: AngularFirestore ) { }

    agregarPaciente(pacientes:any):Promise<any> {
        return this.firestore.collection('pacientes').add(pacientes);
    }

    eliminarPaciente(id:string):Promise<any> {
        return this.firestore.collection('pacientes').doc(id).delete;
    }

    editarPaciente(id:string,data:any):Promise<any> {
        return this.firestore.collection('pacientes').doc(id).update(data);
    }




  }  